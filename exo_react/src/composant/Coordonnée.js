import React,{useState}from 'react';
import { MapContainer, TileLayer, Marker, Popup } from 'react-leaflet'
import '../style/App.css'
import Info from './Info'
import Weather from './Weather'
import L from "leaflet";



let marque = require('./Simplon.json')
const position = [43.5640,2.2806]
function LeafletMap(){
    var greenIcon = new L.Icon({
    iconUrl: 'https://raw.githubusercontent.com/pointhi/leaflet-color-markers/master/img/marker-icon-red.png',
    shadowUrl: 'https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.7/images/marker-shadow.png',
    iconSize: [25, 41],
    iconAnchor: [12, 41],
    popupAnchor: [1, -34],
    shadowSize: [41, 41]
  });
    const [i, setI] = useState(0);
    function changerIndex(index) {
        setI(index)
    }

    const [g, setg] = useState([0,0]);

if("geolocation" in navigator){
    
    navigator.geolocation.getCurrentPosition(function (position) {
        let a = position.coords.latitude;
        let b = position.coords.longitude;
        console.log("geo in nav");
    
         setg([a,b])
    
      })}
      const [weather, setWeather] = useState(undefined)
      function getWeather(lat, long) {
        fetch(
          "https://api.openweathermap.org/data/2.5/weather?lat=" +
            lat +
            "&lon=" +
            long +
            "&units=metric&lang=fr&appid=95a40b54331210f1bcdbd1d4d2597ebf"
        )
          .then((response) => response.json())
          .then((promise) => {
            let jsonTab = promise;
            console.log(jsonTab);
            setWeather(jsonTab);
          })
          .catch((err) => {
            console.log(err);
          });
      }



        return(
            <div id="map">
            <MapContainer center={position} zoom={7} scrollWheelZoom={true}>
                <TileLayer
                attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
                url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                />
                {
                    marque.map(({coordonnee,ville,id,nom}) =>
                        
                        <Marker icon={greenIcon}position={[coordonnee.lat, coordonnee.long]} key={ville}
                        eventHandlers={{
                            click: () => {
                                changerIndex(id)
                                getWeather(coordonnee.lat,coordonnee.long)
                            },
                        }}
                        >
                        
                          
                            <Popup>
                            <h3>{nom}</h3>
                            </Popup>
                            
                        </Marker>
        
                    )}
                    <Marker position={g}>
                        <Popup>
                            vous etes ici
                        </Popup>
                    </Marker>
                
    
            </MapContainer>
            <Info num={i}></Info> 
            </div>
            
        );
     }                                         
    
    export default LeafletMap; 

    

    