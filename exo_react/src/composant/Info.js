import fabriques from '../composant/Simplon.json'
function Info(props){
    return(
        <div className='div-Info'>
            <div className='Info'>
            <h2 className='Ville'>{fabriques[props.num].nom}</h2>
            <p>{fabriques[props.num].info}</p>
            </div>
        </div>
    )
}

export default Info