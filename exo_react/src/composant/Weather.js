import React, { useState } from 'react';

function Weather(coordonnee){
  const [meteo, setMeteo] = useState(undefined)
  let montab = [];
  let latText = 'lat=' + coordonnee.latitude + '&'
  let longText = 'lon=' + coordonnee.longitude + '&'
  let lien = 'https://api.openweathermap.org/data/2.5/weather?' + latText + longText + 'appid=ad8a3899307f8a8df0e4f905b0983b3c&units=metric'
  fetch(lien)
  .then(function(response) {
      response.json().then((data) => {
        montab = data
        console.log(montab.wind,montab.main,montab.name)
        setMeteo(montab)
        
}); 

    
  });
  return(
    <div>
      { (meteo.weather)? meteo.weather[0].main : ""}
    </div>
  )

}


export default Weather;
